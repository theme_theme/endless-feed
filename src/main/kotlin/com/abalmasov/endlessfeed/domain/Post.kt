package com.abalmasov.endlessfeed.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.ManyToOne


@Entity
data class Post (
        @Id @GeneratedValue
        val id: Long,
        @ManyToOne
        var author: User,
        var title: String,
        var text: String
)