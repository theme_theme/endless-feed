package com.abalmasov.endlessfeed.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.ManyToMany


@Entity
data class User(
        @Id @GeneratedValue
        val id: Long,
        val name: String,
        val info: String,
        @ManyToMany
        var friends:List<User>
)