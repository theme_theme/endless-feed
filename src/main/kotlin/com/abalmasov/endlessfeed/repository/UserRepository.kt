package com.abalmasov.endlessfeed.repository

import com.abalmasov.endlessfeed.domain.Post
import com.abalmasov.endlessfeed.domain.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<User, Long>