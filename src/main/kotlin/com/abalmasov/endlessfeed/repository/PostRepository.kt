package com.abalmasov.endlessfeed.repository

import com.abalmasov.endlessfeed.domain.Post
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PostRepository : CrudRepository<Post, Long>