package com.abalmasov.endlessfeed.api

import com.abalmasov.endlessfeed.api.ApiUtil.PREFIX
import com.abalmasov.endlessfeed.domain.User
import com.abalmasov.endlessfeed.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
class UserApiController {
    @Autowired
    lateinit var repository: UserRepository

    @GetMapping(value = PREFIX +"/user/{id}")
    fun getUser(@PathParam("id") id:Long ): User {
        return repository.findOne(id)
    }

    @PostMapping(value = PREFIX +"/user")
    fun postUser(@RequestBody user:User): User {
        return repository.save(user)
    }
}