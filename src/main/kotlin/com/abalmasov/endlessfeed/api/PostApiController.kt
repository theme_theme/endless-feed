package com.abalmasov.endlessfeed.api

import com.abalmasov.endlessfeed.api.ApiUtil.PREFIX
import com.abalmasov.endlessfeed.domain.Post
import com.abalmasov.endlessfeed.repository.PostRepository
import com.abalmasov.endlessfeed.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.websocket.server.PathParam

@RestController
class PostApiController {
    @Autowired
    lateinit var repository: PostRepository

    @GetMapping(value = PREFIX+"/post/{id}")
    fun getPost(@PathParam("id") id:Long ): Post {
        return repository.findOne(id)
    }

    @PostMapping(value = PREFIX+"/post")
    fun postPost(@RequestBody post:Post ): Post {
        return repository.save(post)
    }

    @GetMapping(value = PREFIX+"/stream")
    fun stream(@RequestParam("from") from: Date,
             @RequestParam("size") size: Int): List<Post> {
//       TODO: get user&friends post
        return repository.findAll().toList()
    }
}