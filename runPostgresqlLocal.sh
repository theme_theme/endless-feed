#!/usr/bin/env sh
docker kill postgresql
docker rm -f postgresql
docker rmi -f postgresql
docker run --name postgresql -itd --restart always \
  --publish 5432:5432 \
  --env 'DB_USER=endlessfeed'  --env 'DB_PASS=passw0rd' \
  --env 'PG_PASSWORD=passw0rd' --env 'DB_NAME=endlessfeed' \
  sameersbn/postgresql:9.6-2
docker ps

